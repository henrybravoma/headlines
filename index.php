<?php 
header('Content-type: text/html; charset=utf-8'); 
error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>headline scramble game!</title>
<!-- external style sheet -->
<link rel="stylesheet" type="text/css" href="css/headlinescramble.css" /> 
<!-- external javascript -->
<script language="JavaScript" type="text/javascript" src="inc/prototype.js">
</script>
<script language="JavaScript" type="text/javascript" src="inc/headlinescramble.js">
</script>
</head>
<!-- body with javascript hook - call function when page is finished loading -->
<body onload="init()">
<?php
require_once("inc/rss_fetch.inc");
define('MAGPIE_OUTPUT_ENCODING', 'UTF-8');
$word = $_GET['word'];
if (!isset($word)) $word = "game";
$word = urlencode($word);
$url = "http://news.google.com/news?hl=en&ned=us&q=$word&ie=UTF-8&output=rss";
// swedish feed for better unicode testing
// $url = "http://news.google.com/news?ned=sv_se&hl=sv&ned=sv_se&q=$word&ie=UTF-8&output=rss";
$rss = fetch_rss($url);
foreach ($rss->items as $item) {
  $title = $item['title'];
  $urlsrc = $item['link'];
  echo "<div class=\"headline\">$title</div>\n";
}
?>
<div id="main">
<div id="title"><h1>Headlines scramble game</h1></div>
<div id="words"></div>
<div id="source"></div>
<div id="form">
<form method="get">
search word: <input type="text" name="word" maxlength="100" size="50px" value="<?php echo $word?>" />
</form>
</div>
<div id="check">
<?php
$urlsrc = $item['link'];
echo "<a href=$urlsrc target=\"_blank\" title=\"goto to the source website\">link</a>"; 
?>  
<a href="javascript:check()">check headline</a>    <a href="javascript:window.location.reload()">change source</a>
</div>
<div id="help">
<h1>how to play</h1>
<p>1. Type a keyword in the <font color="red">search word</font> field and press enter</p>
<p>2. Try to guess the scrambeled headline by ordering the words to the original text.</p>
<p>3. Press <font color="red">check headline</font> to view the original headline</p>
<p>	  reload the page by pressing the <font color="red">change source</font> button for a new headline source with the same keyword</p>
<p>4. Enter new keyword to keep playing</p>
<h4><em>date: 2006<br />tools: prototype.js + MagpieRSS and google news<br />This webapp was a result of the 'programing for media designers' course given by 
Michael Murtaugh (automatist.org) at Willem de Kooning Academie - Rotterdam</em></h4>
</div>
</div>
</body>
</html>